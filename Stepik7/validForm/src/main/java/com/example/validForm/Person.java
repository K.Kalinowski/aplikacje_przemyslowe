package com.example.validForm;

import com.example.validForm.Validators.Salary;
import com.example.validForm.Validators.ZipCode;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class Person {
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be start at least two characters")
    private String name;

    @NotNull(message = "Age is required")
    @Min(value = 0, message = "Age must be at least zero")
    private int age;

    @NotNull(message = "Zip code is required")
    @ZipCode
    private String zipCode;

    @NotNull(message = "Salary is required")
    @Salary
    private double salary;

    @AssertTrue(message = "Must accept conditions")
    private boolean acceptTerms;
}
