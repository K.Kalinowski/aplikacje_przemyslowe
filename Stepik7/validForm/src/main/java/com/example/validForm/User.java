package com.example.validForm;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
public class User {
    private int id;
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be at least two characters long")
    private String name;
    @NotNull(message = "Age is required")
    @Min(value = 1, message = "Age must be greater than zero")
    private int age;
    @NotNull(message = "User type is required")
    private UserType userType;

    private Date registrationDate;

    public User() {}

    public User(int id, String name, int age, UserType userType, Date registrationDate) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.userType = userType;
        this.registrationDate = registrationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public static enum UserType {
        GUEST, REGISTERED, ADMIN
    }

    public int getDaysSinceRegistration() {
        Date now = new Date();
        return (int) ((now.getTime() - registrationDate.getTime()) / (1000 * 60 * 60 * 24));
    }
}
