package com.example.validForm.Validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
public class ZipCodeValidator implements ConstraintValidator<ZipCode, String> {
    @Override
    public void initialize(ZipCode constraint) {}

    @Override
    public boolean isValid(String code, ConstraintValidatorContext constraintValidatorContext) {

        return code.matches("\\d{2}-\\d{3}");

    }
}
