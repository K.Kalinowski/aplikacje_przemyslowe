package com.example.validForm.Controllers;

import com.example.validForm.Person;
import com.example.validForm.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class UserController {

    public List<User> userList;

    public UserController() {
        this.userList = new ArrayList<User>(Arrays.asList(
                new User(1, "Maria", 20, User.UserType.ADMIN, new Date()),
                new User(2, "John", 21, User.UserType.GUEST, new Date()),
                new User(3, "Artur", 29, User.UserType.REGISTERED, new Date()),
                new User(4, "Martin", 34, User.UserType.GUEST, new Date()),
                new User(5, "Joe", 40, User.UserType.ADMIN, new Date())));
    }

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("userList", userList);
        return "list";
    }

    @GetMapping("/form")
    public String personForm(Model model){
        model.addAttribute("user", new User());
        return "form";
    }

    @PostMapping("/form")
    public String processOrder(@Valid User user, Errors errors){
        if(errors.hasErrors()){
            return "form";
        }
        user.setId(userList.size()+1);
        user.setRegistrationDate(new Date());
        userList.add(user);
        System.out.println("User created: " + user);
        return "redirect:/list";
    }
}
