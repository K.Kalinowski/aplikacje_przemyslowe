package com.example.validForm.Validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
public class SalaryValidator implements ConstraintValidator<Salary, Double> {
    @Override
    public void initialize(Salary constraint) {}

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext constraintValidatorContext) {

        return value > 2000 && value < 3000;

    }
}
