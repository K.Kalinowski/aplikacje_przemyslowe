package com.tje.users;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class UsersController {

    @GetMapping("/")
    public String home(Model model) throws ParseException {
        String startDateString = "20/05/2007 07:32";
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date startDate = df.parse(startDateString);
        User user = new User(2, "Artur", 29, User.UserType.ADMIN, startDate);
        model.addAttribute("user", user);
        return "home";
    }

    @GetMapping("/list")
    public String list(Model model) throws ParseException {
        List<User> userList = List.of(
                new User(1, "Maria", 20, User.UserType.ADMIN, new Date()),
                new User(2, "John", 21, User.UserType.GUEST, new Date()),
                new User(3, "Artur", 29, User.UserType.REGISTERED, new Date()),
                new User(4, "Martin", 34, User.UserType.GUEST, new Date()),
                new User(5, "Joe", 40, User.UserType.ADMIN, new Date())
        );
        model.addAttribute("userList", userList);
        return "list";
    }
}
