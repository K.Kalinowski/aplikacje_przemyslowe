package ug.stepik5.zad01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ServletComponentScan
@ImportResource("classpath:beans.xml")
public class Zad01Application {

	public static void main(String[] args) {
		SpringApplication.run(Zad01Application.class, args);
	}

}
