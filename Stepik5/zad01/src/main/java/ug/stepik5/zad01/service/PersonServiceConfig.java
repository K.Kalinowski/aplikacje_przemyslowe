package ug.stepik5.zad01.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ug.stepik5.zad01.domain.Person;

@Configuration
public class PersonServiceConfig {

  @Bean
  public Person president() {
    return new Person("President", "Chrystal", "Havoc", "chavocr@yahoo.com", "Mymm");
  }

  @Bean
  public Person vicePresident() {
    return new Person("VicePresiden", "Halley", "Gadaud", "hgadaud9@sohu.com", "Oyope");
  }

  @Bean
  public Person secretary() {
    return new Person("Secretary", "Kirbie", "Wrettum", "kwrettumj@slideshare.net", "Browsetype");
  }

}
