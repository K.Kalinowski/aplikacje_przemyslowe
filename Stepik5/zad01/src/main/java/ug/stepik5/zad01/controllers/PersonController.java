package ug.stepik5.zad01.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ug.stepik5.zad01.domain.Person;
import ug.stepik5.zad01.service.PersonService;

import java.util.ArrayList;

@RestController
public class PersonController {
    final private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/people")
    ArrayList<Person> getAll() {
        return personService.getAll();
    }

    @GetMapping("/people/{id}")
    Person getOne(@PathVariable String id) {
        return personService.getOne(id);
    }

    @PostMapping("/people")
    Person createPerson(@RequestBody Person newPerson) {
        return personService.ceratePerson(newPerson);
    }

    @PutMapping("/people/{id}")
    Person replacePerson(@RequestBody Person newPerson, @PathVariable String id) {
        return personService.replacePerson(newPerson, id);
    }

    @DeleteMapping("/people/{id}")
    void deletePerson(@PathVariable String id) {
        personService.deletePerson(id);
    }
}