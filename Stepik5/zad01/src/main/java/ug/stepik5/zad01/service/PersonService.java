package ug.stepik5.zad01.service;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import ug.stepik5.zad01.domain.Person;

@Service
public class PersonService {
    private ArrayList<Person> personList = new ArrayList<>();

    @Autowired
    public PersonService() {
        loadUsersFromXml();
    }

    private void loadUsersFromXml() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        Map<String, Person> allPeople = context.getBeansOfType(Person.class);
        personList.addAll(allPeople.values());
    }

    public ArrayList<Person> getAll() {
        return personList;
    }

    public Person getOne(String id) {
        for (Person person : personList) {
            if (person.getId().equals(id))
                return person;
        }
        return null;
    }

    public Person ceratePerson(Person newPerson) {
        newPerson.setId(UUID.randomUUID().toString());
        personList.add(newPerson);
        return newPerson;
    }

    public Person replacePerson(Person newPerson, String id) {
        for (Person person : personList) {
            if (person.getId().equals(id)) {
                person.setFirstName(newPerson.getFirstName());
                person.setLastName(newPerson.getLastName());
                person.setEmail(newPerson.getEmail());
                person.setCompanyName(newPerson.getCompanyName());

                return person;
            }
        }

        return null;
    }

    public void deletePerson(String id) {
        personList.removeIf(person -> person.getId().equals(id));
    }
}