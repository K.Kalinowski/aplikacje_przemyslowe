package ug.stepik5.zad02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class Zad02Application {

	public static void main(String[] args) {
		SpringApplication.run(Zad02Application.class, args);
	}

}
