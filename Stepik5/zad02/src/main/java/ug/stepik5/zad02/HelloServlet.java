package ug.stepik5.zad02;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends HttpServlet {
  private static final Logger logger = LoggerFactory.getLogger(HelloServlet.class);

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    logger.info("doGet method called");
    doAction(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    logger.info("doPost method called");
    doAction(request, response);
  }

  private void doAction(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/json");

    String name = request.getParameter("name");
    if (name == null) {
      name = "";
    }

    String message = "Witaj " + name + "!!!";

    try (PrintWriter out = response.getWriter()) {
      out.println("{ \"message\": \"" + message + "\" }");
    }

    logger.info("doAction method called with name: " + name);
  }
}


