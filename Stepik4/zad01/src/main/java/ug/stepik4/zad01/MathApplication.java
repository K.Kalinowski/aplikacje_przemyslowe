package ug.stepik4.zad01;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MathApplication {

	public static void main(String[] args) {
		SpringApplication.run(MathApplication.class, args);
	}

	@Bean
	public CommandLineRunner PrintBeans(ApplicationContext context) {
		return args -> {
			Calculator calculator = context.getBean(Calculator.class);
			System.out.println("Addition: "+calculator.addition(1,1));
			System.out.println("Subtraction: "+calculator.subtraction(1,1));
		};
	}

}
