package ug.stepik4.zad02;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class Zad02Application {

	public static void main(String[] args) {
		SpringApplication.run(Zad02Application.class, args);
	}

	@Bean
	public CommandLineRunner Sort(ApplicationContext context) {
		return args -> {
			SortManager sortManager = context.getBean(SortManager.class);
			int[] array = {14, 5, 2, 10, 4};
			System.out.println("Before sort: "+ Arrays.toString(array));
			sortManager.sort(array);
			System.out.println("After sort: "+ Arrays.toString(array));
		};
	}

}
