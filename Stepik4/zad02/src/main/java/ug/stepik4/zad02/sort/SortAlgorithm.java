package ug.stepik4.zad02.sort;

public interface SortAlgorithm {
    void sort(int[] array);
}
