package ug.stepik4.zad02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ug.stepik4.zad02.sort.SortAlgorithm;

@Component
@Qualifier("sortManager")
public class SortManager {
    @Autowired @Qualifier("bubblesort")
    SortAlgorithm sortAlgorithm;

    public void sort(int[] array) {
        sortAlgorithm.sort(array);
    }
}
