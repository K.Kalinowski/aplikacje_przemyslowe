package ug.stepik4.zad03.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ug.stepik4.zad03.domain.Person;

@Configuration
public class PersonServiceConfig {

  @Bean
  public Person president() {
    return new Person("Chrystal", "Havoc", "chavocr@yahoo.com", "Mymm");
  }

  @Bean
  public Person vicePresident() {
    return new Person("Halley", "Gadaud", "hgadaud9@sohu.com", "Oyope");
  }

  @Bean
  public Person secretary() {
    return new Person("Kirbie", "Wrettum", "kwrettumj@slideshare.net", "Browsetype");
  }

}
