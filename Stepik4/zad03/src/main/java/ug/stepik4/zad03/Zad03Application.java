package ug.stepik4.zad03;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import ug.stepik4.zad03.domain.Person;

import java.util.Map;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class Zad03Application {

	public static void main(String[] args) {
		SpringApplication.run(Zad03Application.class, args);
	}

	@Bean
	public CommandLineRunner PrintBeans(ApplicationContext context) {
		return args -> {
			Map<String, Person> allPeople = context.getBeansOfType(Person.class);
			for (String person : allPeople.keySet()) {
				System.out.println(person + ": " + allPeople.get(person));
			}
		};
	}
}
